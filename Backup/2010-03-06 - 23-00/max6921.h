#ifndef _MAX6921_H_
#define _MAX6921_H_

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <stdint.h>
#include "VFD.h"
#include "timeout.h"

#define MAX_LOAD		PB0
#define MAX_BLANK		PB1
#define MAX_DI			PB3
#define MAX_DO			PB4
#define MAX_CLK			PB5

/** Size of the circular receive buffer, must be power of 2 */
#define MAX_BUFFER_SIZE		16
#define MAX_BUFFER_MASK		( MAX_BUFFER_SIZE - 1)

// Allows accessing as max_char.segs and max_char.grids or
// max_char.byte[0], max_char.byte[1] and max_char.byte[2]
typedef union {
	struct {
		uint8_t segs;
		uint16_t grids;
	};
	uint8_t byte[3];
} max_char;

// Initialise  all the hardware and buffers 
void max_init(void);

// Stops the multiplexing action and set a single char in the display
void max_set(uint8_t segs, uint16_t grids);

// Clears the display
void max_clear();

// Just a macro to display all segments in the VFD
#define max_setall() max_set(0xFF, 0x1FF)

// Sets the brightness of the VFD, by applying a PWM on blank pin
void max_brightness(uint8_t duty);

// Display a string of up to 16 char in the VFD
// Dots ('.') are not counted as the length, as it is integrated
// with the previous char.
// Strings are null (/0) terminated
void max_setstring(const char *str);

// Same as above, but the string is in the flash memory
void max_setstring_P(const char *str);

#endif
