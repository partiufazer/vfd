#include "max6921.h"

volatile struct {
	max_char buffer[MAX_BUFFER_SIZE];
	uint8_t length;
	uint8_t	head;
	uint8_t tail;
} max_buffer;



void max_init(void)
{
	// All pins, except MISO (PB4), as outputs. Pull-up on MISO;
	// PB0 is load, PB1 is blank -> MAX6921
	PORTB |= (1<<MAX_DO);
	DDRB |= (1<<MAX_LOAD) | (1<<MAX_BLANK) | (1<<MAX_DI) | (1<<MAX_CLK);

	// Setup timer 1 for PWM on blank pin (PB1)


	// Configura SPI para modo 0 - Master - fck/64
	SPCR = (1<<SPE) | (1<<MSTR) | (1<<SPR1);

	max_buffer.length = 0;
	max_buffer.head = 0;
	max_buffer.tail = 0;
}

