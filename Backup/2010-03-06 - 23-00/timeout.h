#ifndef _TIMEOUT_H_
#define _TIMEOUT_H_

#include <avr/io.h>
#include <avr/interrupt.h>
#include "VFD.h"

// Defines para timeout da serial
#define TO_TICKS					(5000 / 22)		// 5000 ms

extern volatile uint8_t TOTicks;
extern volatile uint8_t TimeOut;

#endif
