#ifndef _INDUTOR_H_
#define _INDUTOR_H_

#define FALSE	0
#define TRUE	1

// Pulse on PD4, oscilloscope trigger
#define OSC_TRIGGER 	PORTD |= (1<<PD4); PORTD &= ~(1<<PD4)

// Tens�es m�ximas e m�nimas de entrada
#define VI_CONV_FACT	(12 * 1024UL) / (12 + 330) / 1.1
#define MIN_VI_COUNT	(uint16_t)(8 * VI_CONV_FACT)
#define MAX_VI_COUNT	(uint16_t)(16 * VI_CONV_FACT)

// Tens�o de sa�da
#define VO_CONV_FACT	(3.9 * 1024) / (3.9 + 330) / 1.1

// Defines da m�quina de estado
#define SM_IDLE			0
#define SM_START		1
#define SM_GET_CMD		2
#define SM_WRITE		3
#define SM_WR_SP		4
#define SM_WR_KP		5
#define SM_WR_KI		6
#define SM_READ			7
#define SM_RD_SP		8
#define SM_RD_VI		9
#define SM_RD_VO		10
#define SM_END			11
#define SM_ERROR		12
#define SM_WR_LD		13

/* 
	Todas as mensagens iniciam com o caracter 'b', seguido por um caracter
de comando ('r', para comandos de leitura, ou 'w', para comandos de escrita), 
seguidos de seus par�metros. Em nenhum a string possui caracteres terminadores,
como o /0 do C.

Escrita:
	- Atualmente, somente o set-point poder� ser modificado, ent�o o �nico
	  comando de escrita � 's', seguido do 

	  O valor do set-point dever� ser enviado como uma string, com valor em formato 6.2
	  (basicamente basta multiplicar por 4) e deve ter valor entre 20 e 60 volts. Todos
	  os 3 caracteres do valor devem ser enviados.

	  Exemplo de comando para mudan�a de set-point para 20 V:  bws080
	  Exemplo de comando para mudan�a de set-point para 40 V:  bws160


Leitura:
	- Temos 3 comandos de leitura:

*/
#define CB_INICIO		'b'
#define CB_CMD_RD		'r'
#define CB_CMD_WR		'w'
#define CB_WR_SP		's'
#define CB_WR_KP		'p'
#define CB_WR_KI		'i'
#define CB_WR_LD		'l'
#define CB_RD_SP		's'
#define CB_RD_VI		'i'
#define CB_RD_VO		'o'
#define CB_FINAL_MSG	'!'

#define CB_ERROR_MSG	'?'
#define CB_ERROR_START	1		// Caracter de in�cio inv�lido
#define CB_ERROR_CMD	2		// Caracter de comando inv�lido
#define CB_ERROR_SP		3		// Valor de Set Point inv�lido
#define CB_ERROR_WR		4		// Caracter de escrita inv�lido
#define CB_ERROR_RD		5		// Caracter de leitura inv�lido
#define CB_ERROR_TO		6		// Timeout na serial

// Defines para timeout da serial
#define TIMEOUT_PACKET_TIMEOUTTICKS	(5000 / 22)		// 5000 ms


/* typedefs */
typedef struct /* saturating PI controller coefficients */
{
  volatile int16_t w_kp_pu;   /* per-unit proportional gain term. must be limited between 0 and +(4PU-1) */
  volatile int16_t w_ki_pu;   /* per-unit integral gain term. must be limited between 0 and +(4PU-1) */
  int32_t l_integrator_dpu;   /* accumulator for discrete-time integrator. must be limited to +/-(16DPU-1) */
  volatile int16_t sp;
  volatile int16_t pv;
} tPIsat;

#define RENORMALISE         (13)                /* exponent to renormalise after pu calculation */
#define ONE_PU              (8192)              /* = 2^13 */
#define HALF_PU_MINUS_1     (0x0FFF)
#define TWO_PU_MINUS_1      (0x3FFF)
#define THREE_PU_MINUS_1    (0x5FFF)
#define FOUR_PU_MINUS_1     (0x7FFF)
#define TWO_DPU_MINUS_1     (0x07FFFFFF)
#define THREE_DPU_MINUS_1   (0x0BFFFFFF)
#define FOUR_DPU_MINUS_1    (0x0FFFFFFF)
#define EIGHT_DPU_MINUS_1   (0x1FFFFFFF)
#define TWELVE_DPU_MINUS_1  (0x2FFFFFFF)
#define SIXTEEN_DPU_MINUS_1 (0x3FFFFFFF)

/* return x limited to between ll and ul */
#define sat_l(x, ll, ul) ( (x) >= (int32_t)(ul) ) ? (int32_t)(ul) : ( ( (x) <= (int32_t)(ll) ) ? (int32_t)(ll) : (x) )
#define dtos(x) ( ( (x) << ( 16 - RENORMALISE) ) >> 16 ) 
#define stod(x) ( (int32_t)(x) << RENORMALISE ) 

static __inline__ int16_t fw_PI_controller (const int16_t w_error_pu,
                                 tPIsat *tPIsat_params,
                          const int16_t w_ll,
                          const int16_t w_ul);   /* saturating PI controller */



static __inline__ void HW_Init (void);
static __inline__ void ADC_Init (void);

#endif
