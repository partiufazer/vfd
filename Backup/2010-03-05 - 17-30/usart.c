#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdlib.h>
#include "usart.h"

/* size of RX/TX buffers */
#define UART_RX_BUFFER_MASK ( UART_RX_BUFFER_SIZE - 1)
#define UART_TX_BUFFER_MASK ( UART_TX_BUFFER_SIZE - 1)

#if ( UART_RX_BUFFER_SIZE & UART_RX_BUFFER_MASK )
#error RX buffer size is not a power of 2
#endif
#if ( UART_TX_BUFFER_SIZE & UART_TX_BUFFER_MASK )
#error TX buffer size is not a power of 2
#endif

/**  module global variables */
static volatile unsigned char UART_TxBuf[UART_TX_BUFFER_SIZE];
static volatile unsigned char UART_RxBuf[UART_RX_BUFFER_SIZE];
static volatile unsigned char UART_TxHead;
static volatile unsigned char UART_TxTail;
static volatile unsigned char UART_RxHead;
static volatile unsigned char UART_RxTail;
static volatile unsigned char UART_LastRxError;


void uart_init( unsigned int ubrr)
{
    UART_TxHead = 0;
    UART_TxTail = 0;
    UART_RxHead = 0;
    UART_RxTail = 0;
    
    /*Set baud rate */
    UBRR0H = (unsigned char)(ubrr>>8);
    UBRR0L = (unsigned char)ubrr;
    
    /*Enable receiver and transmitter plus corresponding interrupts*/
    UCSR0B = (1<<RXCIE0)|(1<<RXEN0)|(1<<TXEN0);
    
    /* Set frame format: 8data, 1stop bit */
    UCSR0C = (1<<UCSZ00) | (1<<UCSZ01);
}

//#pragma vector = USART_RX_vect
//__interrupt void USART_RX_interrupt(void)
ISR(USART_RX_vect)
{
    unsigned char tmphead;
    unsigned char byte;
    unsigned char usr;
    unsigned char lastRxError;
 
    /* read UART status register and UART data register */ 
    usr  = UCSR0A;
    byte = UDR0;
    
    lastRxError = (usr & ((1<<FE0)|(1<<DOR0)|(1<<UPE0)));
        
    /* calculate buffer index */ 
    tmphead = ( UART_RxHead + 1) & UART_RX_BUFFER_MASK;
    
    if ( tmphead == UART_RxTail ) 
    {
        /* error: receive buffer overflow */
        lastRxError = UART_BUFFER_OVERFLOW >> 8;
    }
    else
    {
        /* store new index */
        UART_RxHead = tmphead;
        /* store received data in buffer */
        UART_RxBuf[tmphead] = byte;
    }
    UART_LastRxError = lastRxError;  
}

ISR(USART_UDRE_vect)
{
	unsigned char tmptail;

	/* if buffer NOT empty */
	if ( (UART_TxHead) != (UART_TxTail)) 
    {
        /* calculate and store new buffer index */
        tmptail = (UART_TxTail + 1) & UART_TX_BUFFER_MASK;
        UART_TxTail = tmptail;

        /* get one byte from buffer and write it to UART */
        UDR0 = UART_TxBuf[tmptail];  /* start transmission */
    }
	else
	{
		/* Disables TX Buffer Empty Interrupt */
		UCSR0B &= ~(1<<UDRIE0);
	}

}

/*************************************************************************
Function: uart_getc()
Purpose:  return byte from ringbuffer  
Returns:  lower byte:  received byte from ringbuffer
          higher byte: last receive error
**************************************************************************/
unsigned int uart_getc(void)
{    
    unsigned char tmptail;
    unsigned char databyte;


    if ( UART_RxHead == UART_RxTail ) {
        return UART_NO_DATA;   /* no data available */
    }
    
    /* calculate /store buffer index */
    tmptail = (UART_RxTail + 1) & UART_RX_BUFFER_MASK;
    UART_RxTail = tmptail; 
    
    /* get data from receive buffer */
    databyte = UART_RxBuf[tmptail];
    
    return (UART_LastRxError << 8) + databyte;

}/* uart_getc */


/*************************************************************************
Function: uart_putc()
Purpose:  write byte to ringbuffer for transmitting via UART
Input:    byte to be transmitted
Returns:  none          
**************************************************************************/
void uart_putc(unsigned char byte)
{
    unsigned char tmphead;
//	unsigned char tmptail;

    tmphead  = (UART_TxHead + 1) & UART_TX_BUFFER_MASK;
    
    while ( tmphead == UART_TxTail ){
        ;/* wait for free space in buffer */
    }
    
    UART_TxBuf[tmphead] = byte;
    UART_TxHead = tmphead;
	
	UCSR0B |= (1<<UDRIE0);
	/* Wait for empty transmit buffer */
//	while ( !( UCSRA & (1<<UDRE)) );
	
//	if ( (UART_TxHead) != (UART_TxTail)) 
//    {
        /* calculate and store new buffer index */
//        tmptail = (UART_TxTail + 1) & UART_TX_BUFFER_MASK;
//        UART_TxTail = tmptail;

        /* get one byte from buffer and write it to UART */
//        UDR = UART_TxBuf[tmptail];  /* start transmission */
//    }

	
}/* uart_putc */


/*************************************************************************
Function: uart_puts()
Purpose:  transmit string to UART
Input:    string to be transmitted
Returns:  none          
**************************************************************************/
void uart_puts(const char *s )
{
    while (*s) 
      uart_putc(*s++);

}/* uart_puts */


/*************************************************************************
Function: uart_buffer_empty()
Purpose:  check if receive buffer is empty
Input:    none
Returns:  0 if RX buffer has data, 1 if RX buffer is emtpy
**************************************************************************/
unsigned char uart_rx_buffer_empty(void)
{
    if ( UART_RxHead == UART_RxTail ) {
        return 1;   /* no data available */
    }
	else {
		return 0;
	}
}/*uart_rx_buffer_empty*/
