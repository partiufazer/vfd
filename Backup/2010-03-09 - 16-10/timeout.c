#include "timeout.h"

volatile uint8_t TOTicks;
volatile uint8_t TimeOut;

// Gera os timeouts para a porta serial
ISR(TIMER2_OVF_vect)
{
	if (TOTicks++ == TO_TICKS)
	{
		TOTicks = 0;
		TimeOut	= TRUE;
	}
}
