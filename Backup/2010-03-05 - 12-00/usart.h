#ifndef _USART_H_
#define _USART_H_

#define USART_BAUD_RATE                                   9600
#define MY_UBRR                                           (((F_CPU / (USART_BAUD_RATE * 16UL))) - 1) 

/** Size of the circular receive buffer, must be power of 2 */
#ifndef UART_RX_BUFFER_SIZE
#define UART_RX_BUFFER_SIZE 8
#endif

/** Size of the circular transmit buffer, must be power of 2 */
#ifndef UART_TX_BUFFER_SIZE
#define UART_TX_BUFFER_SIZE 32
#endif

/** high byte error return code of uart_getc() */
#define UART_FRAME_ERROR      0x0800              /* Framing Error by UART       */
#define UART_OVERRUN_ERROR    0x0400              /* Overrun condition by UART   */
#define UART_BUFFER_OVERFLOW  0x0200              /* receive ringbuffer overflow */
#define UART_NO_DATA          0x0100              /* no receive data available   */

void uart_init( unsigned int ubrr);
unsigned int uart_getc(void);
void uart_putc(unsigned char data);
void uart_puts(const char *s );
unsigned char uart_rx_buffer_empty(void);


#endif
