#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/eeprom.h>
#include <util/delay.h>
#include <util/atomic.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include "usart.h"
#include "VFD.h"

// Vari�veis globais do controle PI
		 tPIsat  tPIsat_params;
volatile uint16_t VoltInp;

// Vari�veis globais relacionados � serial)
volatile uint8_t PacketTimeOut;
volatile uint8_t PacketTimeOutTicks;
volatile uint8_t CurState = SM_IDLE;
		 uint8_t TipoErro;
		 uint8_t SP_cntr;
		 uint16_t SP_novo;

uint16_t EEMEM ee_PI_SP = 20 * VO_CONV_FACT;
uint16_t EEMEM ee_PI_kp = 1000;
uint16_t EEMEM ee_PI_ki = 20;

int __attribute__((OS_main)) main (void)
{
	uint8_t uart_tmp;
	char buffer[8];

	HW_Init();

	// enquanto a tens�o de entrada estiver abaixo de 8V e acima de 16V, n�o faz nada
	do
	{
		ADCSRA |= (1<<ADSC);
		while (ADCSRA & (1<<ADSC));

	} while ((ADC < MIN_VI_COUNT) || (ADC > MAX_VI_COUNT));

	VoltInp = ADC;

	ADMUX = ((1<<REFS0) | (1<<REFS1)) + 0; 	// ADC0 -> feedback do conversor boost
	ADCSRA |= (1<<ADIF) | (1<<ADIE);		// limpa o flag e habilita interrup��o do ADC

	tPIsat_params.w_kp_pu = eeprom_read_word(&ee_PI_kp);
	tPIsat_params.w_ki_pu = eeprom_read_word(&ee_PI_ki);
	tPIsat_params.sp = eeprom_read_word(&ee_PI_SP);

	sei();

	uart_puts("Inicio VFD\r\n");
	
	ADCSRA |= (1<<ADSC);		// Inicia primeira leitura do ADC

	for(;;)
	{
		// In�cio da m�quina de estados para leitura da serial
		
		// Se o buffer est� vazio, verifica os casos que precisam enviar dados ou se deu timeout
		if (uart_rx_buffer_empty())
		{
			if (PacketTimeOut)
			{
				CurState = SM_ERROR;
				TipoErro = CB_ERROR_TO;
				PacketTimeOutTicks = 0;
				PacketTimeOut = FALSE;
			}

			// Casos em que n�o 
			switch (CurState)
			{
				// Retorna o valor do Set-Point no formato 6.2 (multiplicado por 4)
				case SM_RD_SP:
					ATOMIC_BLOCK(ATOMIC_FORCEON)
					{
						SP_novo = tPIsat_params.sp;
					}					
					sprintf(buffer, "%03d", (SP_novo * 50 + 68) / 136);
					uart_puts(buffer);

					CurState = SM_END;
					break;

				case SM_RD_VI:
					sprintf(buffer, "%03d", (VoltInp * 10 + 41) / 82);
					uart_puts(buffer);

					CurState = SM_END;
					break;

				case SM_RD_VO:
					ATOMIC_BLOCK(ATOMIC_FORCEON)
					{
						SP_novo = tPIsat_params.pv;
					}					

					sprintf(buffer, "%03d", (SP_novo * 100) / 272);
					uart_puts(buffer);
									
					CurState = SM_END;
					break;

				case SM_END:
					TCCR2B = 0;					// Para Timer2
					uart_putc(CB_FINAL_MSG);
					CurState = SM_IDLE;
					break;

				case SM_ERROR:
					TCCR2B = 0;					// Para Timer2
					uart_putc(CB_ERROR_MSG);
					uart_putc(TipoErro + '0');
					CurState = SM_IDLE;
					break;
			}			
		}

		// Se tiver algo no buffer, executa a m�quina de estados.
		else
		{
			// Reseta os timers e contador de timeout para a serial
			TCNT2 = 0;					// Zera o timer 2
			GTCCR |= (1<<PSRASY);		// Zera o prescaler do timer2
			PacketTimeOutTicks = 0;
			PacketTimeOut = FALSE;
			uart_tmp = uart_getc();

			//uart_putc('0' + CurState);

			switch (CurState)
			{
				case SM_IDLE:
					CurState = SM_START;
					TCCR2B = (1<<CS22) | (1<<CS21) | (1<<CS20);	// Inicia Timer2 p/ Timeout
					break;

				// Confere se o primeiro byte � 'b', j� que todos os comandos come�am com ele
				case SM_START:
					if (CB_INICIO == uart_getc())
					{
						CurState = SM_GET_CMD;	
					}
					else
					{
						CurState = SM_ERROR;
						TipoErro = CB_ERROR_START;
					}
					break;
				
				// Segundo byte - byte de comando
				// Verifica se � leitura ou escrita
				case SM_GET_CMD:
					tmp = uart_getc();
					if (CB_CMD_RD == tmp)
					{
						CurState = SM_READ;
					}

					// Se for escrita, tamb�m inicializa as vari�veis para
					// leitura dos novos valores
					else if (CB_CMD_WR == tmp)
					{
						CurState = SM_WRITE;
						SP_cntr = 0;
						SP_novo = 0;
					}
					else
					{
						CurState = SM_ERROR;
						TipoErro = CB_ERROR_CMD;
					}
					break;

				// Veririfica qual vari�vel se quer escrever
				// Atualmente s� pode mexer no set-point
				case SM_WRITE:
					if (CB_WR_SP == uart_getc())
					{
						CurState = SM_WR_SP;
					}
					else
					{
						CurState = SM_ERROR;
						TipoErro = CB_ERROR_CMD;
					}
					break;

				// L� os pr�ximos 3 bytes que ter�o o valor do novo set point
				case SM_WR_SP:
					tmp = uart_getc();

					// s� aceita se for n�mero
					if ((tmp >='0') && (tmp <= '9'))
					{
						SP_novo *= 10;
						SP_novo += tmp - '0';			// converte para valor num�rico
						SP_cntr++;

						if (SP_cntr > 2)				// Se j� leu os 3 bytes
						{
							// verifica se est� dentro da faixa aceit�vel
							if ((SP_novo >= 80 ) && (SP_novo <= 240))
							{
								// Converte e salva o valor do set-point novo
								// Teoricamente deveria multiplicar por 3,92, mas como
								// n�o d�, faz uma leve gambiarra para somente utilizar 
								// n�meros inteiros.
								SP_novo = ((SP_novo * 136 + 25) / 50);

								ATOMIC_BLOCK(ATOMIC_FORCEON)
								{
									tPIsat_params.sp = SP_novo;
								}

								eeprom_write_word(&ee_PI_SP, SP_novo);

								CurState = SM_END;
							}
							else 
							{
								CurState = SM_ERROR;
								TipoErro = CB_ERROR_SP;
							}
						}
					}
					else
					{
						CurState = SM_ERROR;
						TipoErro = CB_ERROR_SP;
					}
					break;

				// Em caso de leitura, verifica qual o valor que se quer ler
				case SM_READ:
					switch (uart_getc())
					{
						case CB_RD_SP:
							CurState = SM_RD_SP;
							break;

						case CB_RD_VI:
							CurState = SM_RD_VI;
							break;

						case CB_RD_VO:
							CurState = SM_RD_VO;
							break;
				
						default:
							CurState = SM_ERROR;
							TipoErro = CB_ERROR_RD;
							break;
					}
					break;
			}
		}
		
	}

}

static __inline__ void HW_Init (void)
{
	// All pins, except MISO (PB4), as outputs. Pull-up on MISO;
	PORTB = (1<<PB4);;
	DDRB = (1<<PB0) | (1<<PB1) | (1<<PB2) | (1<<PB3) | (1<<PB5);

	// RX (PD0) as input and TX (PD1) as output. (OC0A) PD6 as output.
	// All inputs with pull-up
	PORTD = (1<<PD0) | (1<<PD2) | (1<<PD3) | (1<<PD4) | (1<<PD5) | (1<<PD7);
	DDRD = (1<<PD1) | (1<<PD6);

	uart_init(MY_UBRR);

	ADC_Init();

	// Configura Timer 2 -> timeout da serial
	// PS = 1024 -> interrup��o a cada ~21.8 ms, sempre que o timer d� overflow
	// TCCR2B = (1<<CS22) | (1<<CS21) | (1<<CS20); -> s� ligar quando receber alguma coisa
	TIMSK2 = (1<<TOIE2);

	// Configura PWM
	TCCR0A = (1<<COM0A1) | (1<<WGM01) | (1<<WGM00);
	TCCR0B = (1<<CS00);
}

static __inline__ void ADC_Init (void)
{
	PORTC = 0x7C;	// Pull-ups on everything except ADC0 and ADC1;
	DDRC = 0;		// Everything is input;

	// PS = 64 -> f = (18.432M/64) = 288 kHz -> ~20.6 ksps
	// Vref = Int 1.10V
	ADCSRA = (1<<ADEN) | (1<<ADPS2) | (1<<ADPS1);
	ADMUX = ((1<<REFS0) | (1<<REFS1)) + 1; 	// ADC1	-> leitura da tens�o de entrada
	ADCSRA |= (1<<ADSC);					// Descarta a primeira leitura do ADC
	while (ADCSRA & (1<<ADSC));

}



/******************************************************************************

	Code from Artur (adebeun2) from AVRFreaks.net (link below)
	http://www.avrfreaks.net/index.php?name=PNphpBB2&file=viewtopic&p=241636#241636

******************************************************************************/
int16_t fw_PI_controller (const int16_t w_error_pu,
                                 tPIsat *tPIsat_params,
                          const int16_t w_ll,
                          const int16_t w_ul)   /* saturating PI controller */
{
  int32_t l_temp_dpu;                 /* temporary variable */
  int32_t l_proportional_dpu;         /* proportional term, error(k)*Kp */
  int32_t l_integral_dpu;             /* integral term, error(k)*Kp*Ki* */

  /* calculate the proportional term: error(k) * Kp [DPU]
     NOTE: error(k) MUST have been saturated to +/-(4PU-1) BEFORE calling this routine */
  l_proportional_dpu = (int32_t)tPIsat_params->w_kp_pu * (int32_t)w_error_pu; /* (1) */
  /* saturate to +/-(4DPU-1), to allow for rescaling to single precision */
  l_proportional_dpu = sat_l(l_proportional_dpu, -FOUR_DPU_MINUS_1, FOUR_DPU_MINUS_1); /* (1) */

  /* calculate the integrator input term: error(k) * Kp * [DPU] */
  l_temp_dpu = (int32_t)tPIsat_params->w_ki_pu * dtos(l_proportional_dpu); /* (2) */
  /* saturate to +/-(8DPU-1), to prevent the integrator from overflowing */
  l_temp_dpu = sat_l(l_temp_dpu, -EIGHT_DPU_MINUS_1, EIGHT_DPU_MINUS_1); /* (2) */

  /* calculate the integral term: error(k)*Kp*Ki*(1+1/z)/(1-1/z). Because the integrator
     is saturated between +/-(16DPU-1), this term is bounded between +/-24DPU */
  l_integral_dpu = l_temp_dpu + tPIsat_params->l_integrator_dpu; /* (3) */

  /* update the integrator: integrator(k) = integrator(k-1) + 2*error(k)*Kp*Ki
     as l_temp_DPU is saturated to +/-(8DPU-1) this fits into a long word
     if the integrator is limited to +/-(16DPU-1) */
  tPIsat_params->l_integrator_dpu = l_integral_dpu + l_temp_dpu; /* (4) */

  /* saturate the integrator to +/-16DPU */
  tPIsat_params->l_integrator_dpu = sat_l(tPIsat_params->l_integrator_dpu, -SIXTEEN_DPU_MINUS_1, SIXTEEN_DPU_MINUS_1); /* (4) */

  /* calculate the output term: proportional(k) + integral(k) [DPU]
     this is bounded between +/-28DPU */
  l_temp_dpu = l_proportional_dpu + l_integral_dpu; /* (5) */
  /* saturate the output term between the lower limit and the upper limit */
  l_temp_dpu = sat_l(l_temp_dpu, stod(w_ll), stod(w_ul));
  l_temp_dpu = dtos(l_temp_dpu);

  /* assign it to the output variable */
  return((int16_t)l_temp_dpu);
} 


// Gera os timeouts para a porta serial
ISR(TIMER2_OVF_vect)
{
	uint8_t PacketTimeOutTicksLCL = PacketTimeOutTicks;

	if (PacketTimeOutTicksLCL++ == TIMEOUT_PACKET_TIMEOUTTICKS)
	{
		PacketTimeOutTicksLCL = 0;
		PacketTimeOut         = TRUE;
	}
	
	PacketTimeOutTicks = PacketTimeOutTicksLCL;;
}

// Inicia nova convers�o e faz o controle PI
ISR(ADC_vect)
{
	static uint8_t InpCounter;
	int16_t error;

	ADCSRA |= (1<<ADSC);

	tPIsat_params.pv = ADC;
	error =  tPIsat_params.sp - tPIsat_params.pv;
	OCR0A = fw_PI_controller (error, &tPIsat_params, 0, 180);

}
